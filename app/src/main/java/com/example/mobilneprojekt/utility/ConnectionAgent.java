package com.example.mobilneprojekt.utility;

import android.content.Context;
import android.net.ConnectivityManager;

public class ConnectionAgent {

    public static boolean isConnected(Context context) {
        var connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            return false;

        var activeNetworkInfo =  connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}