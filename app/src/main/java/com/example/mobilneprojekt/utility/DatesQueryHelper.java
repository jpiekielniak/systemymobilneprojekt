package com.example.mobilneprojekt.utility;

import android.os.Build;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

public class DatesQueryHelper {

    public static final String[] appointmentArr = {"Dzisiaj", "Jutro", "Najbliższe 10 wizyt", "Najbliższe 20 wizyt", "Najbliższy miesiąc", "Najbliższe 3 miesiące", "Najbliższe pół roku", "Konkretna data"};
    public static final LinkedHashMap<String, String> dateQueryDict;
    static {
        dateQueryDict = new LinkedHashMap<>();
        dateQueryDict.put("Dzisiaj", "1");
        dateQueryDict.put("Jutro", tomorrowDateAsString());
        dateQueryDict.put("Najbliższe 10 wizyt", "10w");
        dateQueryDict.put("Najbliższe 20 wizyt", "20w");
        dateQueryDict.put("Najbliższy miesiąc", "30");
        dateQueryDict.put("Najbliższe 3 miesiące", "90");
        dateQueryDict.put("Najbliższe pół roku", "180");
    }

    private static String tomorrowDateAsString() {
        var pattern = "dd-MM-yyyy";
        var DateFor = new SimpleDateFormat(pattern, Locale.getDefault());
        return DateFor.format(new Date(new Date().getTime() + 86400000));
    }

    public static String todayDateAsString() {
        var pattern = "dd-MM-yyyy";
        var DateFor = new SimpleDateFormat(pattern, Locale.getDefault());
        return DateFor.format(new Date());
    }


    public static String formatDateTime(String datetime_arg) throws ParseException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            var inputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.getDefault());
            var outputDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
            var datetime = inputDateFormat.parse(datetime_arg);
            return outputDateFormat.format(datetime);
        }
        // return without changes if condition not met
        return datetime_arg;
    }
}
