package com.example.mobilneprojekt;

import static com.example.mobilneprojekt.utility.ApiParamNames.CITY;
import static com.example.mobilneprojekt.utility.ApiParamNames.CITY_CODE;
import static com.example.mobilneprojekt.utility.ApiParamNames.FIRST_NAME;
import static com.example.mobilneprojekt.utility.ApiParamNames.HOUSE_NUMBER;
import static com.example.mobilneprojekt.utility.ApiParamNames.ID;
import static com.example.mobilneprojekt.utility.ApiParamNames.LAST_NAME;
import static com.example.mobilneprojekt.utility.ApiParamNames.PHONE;
import static com.example.mobilneprojekt.utility.ApiParamNames.STREET;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;


public class PersonalSettingsActivity extends AppCompatActivity {

    private int userID;

    TextView firstNameLabel;
    TextView lastNameLabel;
    TextView phoneLabel;
    TextView addressLabel;
    TextView firstNameEdit;
    TextView lastNameEdit;
    TextView phoneEdit;
    TextView addressEdit;
    Button firstNameSubmit;
    Button lastNameSubmit;
    Button phoneSubmit;
    Button addressSubmit;
    EditText firstNameInput;
    EditText lastNameInput;
    EditText phoneInput;
    EditText cityCodeInput;
    EditText cityInput;
    EditText streetInput;
    EditText houseNumberInput;
    RelativeLayout firstNameSublayout;
    RelativeLayout lastNameSublayout;
    RelativeLayout phoneSublayout;
    RelativeLayout addressSublayout;
    ArrayList<RelativeLayout> subFormsArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_settings);

        loadSharedPref();


        var toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);


        initBaseInfo();

        firstNameLabel = findViewById(R.id.first_name_edit_current);
        lastNameLabel = findViewById(R.id.last_name_edit_current);
        phoneLabel = findViewById(R.id.phone_edit_current);
        addressLabel = findViewById(R.id.address_edit_current);

        firstNameEdit = findViewById(R.id.first_name_edit_button);
        lastNameEdit = findViewById(R.id.last_name_edit_button);
        phoneEdit = findViewById(R.id.phone_edit_button);
        addressEdit = findViewById(R.id.address_edit_button);

        firstNameSubmit = findViewById(R.id.first_name_edit_submit);
        lastNameSubmit = findViewById(R.id.last_name_edit_submit);
        phoneSubmit = findViewById(R.id.phone_edit_submit);
        addressSubmit = findViewById(R.id.address_edit_submit);

        firstNameInput = findViewById(R.id.first_name_edit_input);
        lastNameInput = findViewById(R.id.last_name_edit_input);
        phoneInput = findViewById(R.id.phone_edit_input);
        cityCodeInput = findViewById(R.id.city_code_edit_input);
        cityInput = findViewById(R.id.city_edit_input);
        streetInput = findViewById(R.id.street_edit_input);
        houseNumberInput = findViewById(R.id.house_number_edit_input);

        firstNameSublayout = findViewById(R.id.first_name_edit_sublayout);
        lastNameSublayout = findViewById(R.id.last_name_edit_sublayout);
        phoneSublayout = findViewById(R.id.phone_edit_sublayout);
        addressSublayout = findViewById(R.id.address_edit_sublayout);

        // init sublayouts array
        initSubFormsArray();

        // onclick listeners for edit buttons (to show/hide edit forms)
        initEditButtonOnClicks();

        // onclick listeners for submit buttons in the sublayouts
        firstNameSubmit.setOnClickListener(button -> {
            String firstname = firstNameInput.getText().toString();
            if (firstname.isEmpty()) {
                Toast.makeText(this, R.string.field_empty_warning, Toast.LENGTH_LONG).show();
                return;
            }
            HashMap<String, String> putParams = new HashMap<>();
            putParams.put(ID, String.valueOf(userID));
            putParams.put(FIRST_NAME, firstname);
            new DataService(this).updatePersonalSettings(putParams, new DataService.JsonObjectResponseListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(PersonalSettingsActivity.this, R.string.field_update_successfull, Toast.LENGTH_LONG).show();
                    firstNameLabel.setText(firstname);
                    firstNameInput.setText(firstname);
                    firstNameSublayout.setVisibility(View.GONE);
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(PersonalSettingsActivity.this, R.string.db_general_error, Toast.LENGTH_LONG).show();
                }
            });
        });
        lastNameSubmit.setOnClickListener(button -> {
            String lastname = lastNameInput.getText().toString();
            if (lastname.isEmpty()) {
                Toast.makeText(this, R.string.field_empty_warning, Toast.LENGTH_LONG).show();
                return;
            }
            HashMap<String, String> putParams = new HashMap<>();
            putParams.put(ID, String.valueOf(userID));
            putParams.put(LAST_NAME, lastname);
            new DataService(this).updatePersonalSettings(putParams, new DataService.JsonObjectResponseListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(PersonalSettingsActivity.this, R.string.field_update_successfull, Toast.LENGTH_LONG).show();
                    lastNameLabel.setText(lastname);
                    lastNameInput.setText(lastname);
                    lastNameSublayout.setVisibility(View.GONE);
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(PersonalSettingsActivity.this, R.string.db_general_error, Toast.LENGTH_LONG).show();
                }
            });

        });
        phoneSubmit.setOnClickListener(button -> {
            var phone = phoneInput.getText().toString();
            if (phone.isEmpty()) {
                Toast.makeText(this, R.string.field_empty_warning, Toast.LENGTH_LONG).show();
                return;
            }
            HashMap<String, String> putParams = new HashMap<>();
            putParams.put(ID, String.valueOf(userID));
            putParams.put(PHONE, phone);
            new DataService(this).updatePersonalSettings(putParams, new DataService.JsonObjectResponseListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(PersonalSettingsActivity.this, R.string.field_update_successfull, Toast.LENGTH_LONG).show();
                    phoneLabel.setText(phone);
                    phoneInput.setText(phone);
                    phoneSublayout.setVisibility(View.GONE);
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(PersonalSettingsActivity.this, R.string.db_general_error, Toast.LENGTH_LONG).show();
                }
            });

        });
        addressSubmit.setOnClickListener(button -> {
            var cityCode = cityCodeInput.getText().toString();
            var city = cityInput.getText().toString();
            var street = streetInput.getText().toString();
            var houseNumber = houseNumberInput.getText().toString();

            if (cityCode.isEmpty() || city.isEmpty() || street.isEmpty() || houseNumber.isEmpty()) {
                Toast.makeText(this, R.string.fields_empty_warning, Toast.LENGTH_LONG).show();
                return;
            }

            HashMap<String, String> putParams = new HashMap<>();
            putParams.put(ID, String.valueOf(userID));
            putParams.put(CITY_CODE, cityCode);
            putParams.put(CITY, city);
            putParams.put(STREET, street);
            putParams.put(HOUSE_NUMBER, houseNumber);
            new DataService(this).updatePersonalSettings(putParams, new DataService.JsonObjectResponseListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(PersonalSettingsActivity.this, R.string.field_update_successfull, Toast.LENGTH_LONG).show();
                    cityCodeInput.setText(cityCode);
                    cityInput.setText(city);
                    streetInput.setText(street);
                    houseNumberInput.setText(houseNumber);
                    addressLabel.setText(String.format(Locale.getDefault(), "%s %s, %s", street, houseNumber, city));
                    addressSublayout.setVisibility(View.GONE);
                }

                @Override
                public void onError(String error) {
                    Toast.makeText(PersonalSettingsActivity.this, R.string.db_general_error, Toast.LENGTH_LONG).show();
                }
            });

        });

    }

    private void initSubFormsArray() {
        subFormsArr = new ArrayList<>();
        subFormsArr.add(firstNameSublayout);
        subFormsArr.add(lastNameSublayout);
        subFormsArr.add(phoneSublayout);
        subFormsArr.add(addressSublayout);
    }

    private void closeAllSubForms(RelativeLayout apartFromThis) {
        for (RelativeLayout relativeLayout : subFormsArr) {
            // close all other subform layouts apart from the one in the argument
            if (relativeLayout.getId() != apartFromThis.getId())
                relativeLayout.setVisibility(View.GONE);

        }
    }

    private void initEditButtonOnClicks() {
        // onclick listenerts for edit buttons (to show edit forms)
        firstNameEdit.setOnClickListener(button -> {
            if (firstNameSublayout.getVisibility() == View.VISIBLE) {
                firstNameSublayout.setVisibility(View.GONE);
            } else {
                firstNameSublayout.setVisibility(View.VISIBLE);
                closeAllSubForms(firstNameSublayout);
            }
        });
        lastNameEdit.setOnClickListener(button -> {
            if (lastNameSublayout.getVisibility() == View.VISIBLE) {
                lastNameSublayout.setVisibility(View.GONE);
            } else {
                lastNameSublayout.setVisibility(View.VISIBLE);
                closeAllSubForms(lastNameSublayout);
            }
        });
        phoneEdit.setOnClickListener(button -> {
            if (phoneSublayout.getVisibility() == View.VISIBLE) {
                phoneSublayout.setVisibility(View.GONE);
            } else {
                phoneSublayout.setVisibility(View.VISIBLE);
                closeAllSubForms(phoneSublayout);
            }
        });
        addressEdit.setOnClickListener(button -> {
            if (addressSublayout.getVisibility() == View.VISIBLE) {
                addressSublayout.setVisibility(View.GONE);
            } else {
                addressSublayout.setVisibility(View.VISIBLE);
                closeAllSubForms(addressSublayout);
            }
        });
    }

    private void loadSharedPref() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        userID = sharedPreferences.getInt(LoginActivity.SHARED_PREF_USER_ID_KEY, -1);
        if (userID == -1)
            finish();

    }


    public void initBaseInfo() {
        new DataService(this).userSettingsInfo(String.valueOf(userID), new DataService.JsonObjectResponseListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    var firstName = response.getString(FIRST_NAME);
                    firstNameLabel.setText(firstName);
                    firstNameInput.setText(firstName);

                    var lastName = response.getString(LAST_NAME);
                    lastNameLabel.setText(lastName);
                    lastNameInput.setText(lastName);

                    var phone = response.getString(PHONE);
                    phoneLabel.setText(phone);
                    phoneInput.setText(phone);

                    var cityCode = response.getString(CITY_CODE);
                    cityCodeInput.setText(cityCode);

                    var city = response.getString(CITY);
                    cityInput.setText(city);

                    var street = response.getString(STREET);
                    streetInput.setText(street);

                    var houseNumber = response.getString(HOUSE_NUMBER);
                    houseNumberInput.setText(houseNumber);

                    addressLabel.setText(String.format(Locale.getDefault(), "%s %s, %s", street, houseNumber, city));

                } catch (JSONException e) {
                    Toast.makeText(
                            PersonalSettingsActivity.this, R.string.db_processing_error, Toast.LENGTH_LONG
                    ).show();
                }
            }

            @Override
            public void onError(String error) {
                Toast.makeText(
                        PersonalSettingsActivity.this, R.string.db_general_error, Toast.LENGTH_LONG
                ).show();
            }
        });
    }

}