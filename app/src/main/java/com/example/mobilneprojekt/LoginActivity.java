package com.example.mobilneprojekt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobilneprojekt.utility.ConnectionAgent;
import com.example.mobilneprojekt.utility.ApiParamNames;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    private EditText loginInput;
    private EditText passwordInput;
    private Button loginButton;
    private TextView tvRedirect;
    private TextView tvRedirectPwdReset;

    public static final String SHARED_PREF_USER_ID_KEY = "SHARED_PREF_USER_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginInput = findViewById(R.id.username_input);
        passwordInput = findViewById(R.id.password_input);
        loginButton = findViewById(R.id.login_button);
        tvRedirect = findViewById(R.id.redirect_to_register);
        tvRedirectPwdReset = findViewById(R.id.redirect_to_pwd_reset);

        loginButton.setOnClickListener(button -> {
            var login = loginInput.getText().toString();
            var password = passwordInput.getText().toString();
            boolean isConnected = ConnectionAgent.isConnected(this);

            if (!isConnected) {
                Toast.makeText(
                        LoginActivity.this, R.string.no_connection_toast, Toast.LENGTH_SHORT
                ).show();

                return;
            }

            var dataService = new DataService(this);
            dataService.logTheUser(login, password, new DataService.JsonObjectResponseListener() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        var isAuthenticated = response.getBoolean(ApiParamNames.BOOLEAN_LOGIN_RESPONSE);
                        if (!isAuthenticated) {
                            Toast.makeText(
                                    LoginActivity.this, R.string.is_not_authenticated, Toast.LENGTH_SHORT
                            ).show();
                            return;
                        }

                        saveData(response.getInt(ApiParamNames.ID));
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();

                    } catch (JSONException exception) {
                        Toast.makeText(
                                LoginActivity.this, R.string.db_processing_error, Toast.LENGTH_SHORT
                        ).show();
                    }
                }

                @Override
                public void onError(String message) {
                    Toast.makeText(
                            LoginActivity.this, R.string.db_general_error, Toast.LENGTH_SHORT
                    ).show();
                }
            });
        });

        tvRedirect.setOnClickListener(view -> {
            startActivity(new Intent(this, RegistrationActivity.class));
            finish();
        });

        tvRedirectPwdReset.setOnClickListener(view -> {
            var intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(DataService.PASSWORD_RESET_URL));
            startActivity(intent);
        });
    }

    private void saveData(int data) {
        var sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SHARED_PREF_USER_ID_KEY, data);
        editor.apply();
    }
}
