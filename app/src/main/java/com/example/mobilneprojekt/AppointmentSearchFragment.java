package com.example.mobilneprojekt;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.mobilneprojekt.utility.ApiParamNames;
import com.example.mobilneprojekt.utility.DatesQueryHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class AppointmentSearchFragment extends Fragment {

    private String[] appointmentArr;
    private Map<String, String> appointmentDict;
    private String[] specialityArr;
    private String[] personelArr;
    private int[] specialityIdArr;
    private int[] personelIdArr;
    private int[] appointmentIdArr;
    private AutoCompleteTextView dropdownSpeciality;
    private AutoCompleteTextView dropdownPersonel;
    private AutoCompleteTextView dropdownAppointment;
    private ArrayAdapter<String> specialityArrayAdapter;
    private ArrayAdapter<String> personelArrayAdapter;
    private ArrayAdapter<String> appointmentArrayAdapter;
    private String personelQ;
    private String appointmentQ;
    private String specialityQ;
    private Button searchButton;

    private DatePickerDialog datePickerDialog;
    private Button datePickerButton;

    private AppointmentSearchFragmentListener listener;

    public AppointmentSearchFragment() { }

    interface AppointmentSearchFragmentListener {
        void onSearchClick(HashMap<String, String> getParams);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        var view = inflater.inflate(R.layout.fragment_appointment_search, container, false);

        datePickerButton = view.findViewById(R.id.date_picker_button);
        initDatePicker();
        datePickerButton.setOnClickListener(button -> {
            openDatePicker();
        });

        initDropDownMenus(view);
        fillDropDownMenus();

        dropdownPersonel.setOnItemClickListener((parent, view1, position, id) -> {
            if (position == personelArr.length - 1)
                personelQ = null;
            else
                personelQ = String.valueOf(personelIdArr[position]);
        });

        dropdownSpeciality.setOnItemClickListener((parent, view12, position, id) -> {
            if (position == specialityArr.length - 1)
                specialityQ = null;
            else
                specialityQ = String.valueOf(personelIdArr[position]);

        });

        dropdownAppointment.setOnItemClickListener((parent, view13, position, id) -> {
            if (appointmentArr.length - 1 == position) {
                datePickerButton.setVisibility(View.VISIBLE);
                appointmentQ = datePickerButton.getText().toString();
            } else {
                datePickerButton.setVisibility(View.GONE);
                appointmentQ = DatesQueryHelper.dateQueryDict.get(appointmentArr[position]);
            }
        });

        searchButton = view.findViewById(R.id.search_appointments_btn);
        searchButton.setOnClickListener(button -> {
            var getParams = new HashMap<String, String>();

            if (personelQ != null)
                getParams.put(ApiParamNames.SEARCH_PERSONEL_ID, personelQ);
            if (appointmentQ != null)
                getParams.put(ApiParamNames.SEARCH_DATE, appointmentQ);
            if (specialityQ != null)
                getParams.put(ApiParamNames.SEARCH_SPECIALITY_ID, specialityQ);

            listener.onSearchClick(getParams);
        });

        return view;
    }

    private void initDatePicker() {
        DatePickerDialog.OnDateSetListener dateSetListener = (view, year, month, dayOfMonth) -> {
            month += 1;
            var date = String.format("%s-%s-%s", dayOfMonth, month, year);
            datePickerButton.setText(date);
            appointmentQ = date;
        };
        var style = AlertDialog.THEME_DEVICE_DEFAULT_DARK;
        var calendar = Calendar.getInstance();
        var year = calendar.get(Calendar.YEAR);
        var month = calendar.get(Calendar.MONTH);
        var day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getContext(), style, dateSetListener, year, month, day);
        datePickerButton.setText(DatesQueryHelper.todayDateAsString());
    }

    public void openDatePicker() {
        datePickerDialog.show();
    }

    public void initDropDownMenus(View view) {
        dropdownSpeciality = view.findViewById(R.id.autoCompleteSpeciality);
        dropdownPersonel = view.findViewById(R.id.autoCompletePersonel);
        appointmentArr = DatesQueryHelper.appointmentArr;
        dropdownAppointment = view.findViewById(R.id.autoCompleteAppointment);
        appointmentArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.list_item, appointmentArr);
        dropdownAppointment.setAdapter(appointmentArrayAdapter);
    }

    public void fillDropDownMenus() {
        var dataService = new DataService(getContext());
        dataService.getBaseInfo(new DataService.JsonObjectResponseListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    var specjalnosc = response.getJSONArray(ApiParamNames.SPECIALITY_ARRAY);
                    var personel = response.getJSONArray(ApiParamNames.PERSONEL_ARRAY);

                    personelArr = new String[personel.length() + 1];
                    personelIdArr = new int[personel.length()];
                    for (int i = 0; i < personel.length(); i++) {
                        var id_data = personel.getJSONArray(i);
                        personelIdArr[i] = id_data.getInt(0);
                        personelArr[i] = id_data.getString(1);
                    }

                    personelArr[personel.length()] = getResources().getString(R.string.personel_all_choice);
                    personelArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.list_item, personelArr);
                    dropdownPersonel.setAdapter(personelArrayAdapter);

                    specialityArr = new String[specjalnosc.length() + 1];
                    specialityIdArr = new int[specjalnosc.length()];
                    for (int i = 0; i < specjalnosc.length(); i++) {
                        JSONArray id_data = specjalnosc.getJSONArray(i);
                        specialityIdArr[i] = id_data.getInt(0);
                        specialityArr[i] = id_data.getString(1);
                    }

                    specialityArr[specjalnosc.length()] = getResources().getString(R.string.speciality_all_choice);
                    specialityArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.list_item, specialityArr);
                    dropdownSpeciality.setAdapter(specialityArrayAdapter);

                } catch (JSONException exception) {
                    exception.printStackTrace();
                    Toast.makeText(getContext(),
                            R.string.db_processing_error, Toast.LENGTH_SHORT
                    ).show();
                }
            }

            @Override
            public void onError(String message) {
                Toast.makeText(getContext(),
                        R.string.db_general_error, Toast.LENGTH_SHORT
                ).show();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AppointmentSearchFragmentListener)
            listener = (AppointmentSearchFragmentListener) context;
        else
            throw new RuntimeException(context.toString() + ": must implement AppointmentSearchFragmentListener");
    }
}
